"   __   _(_)_ __ ___  _ __ ___
"   \ \ / / | '_ ` _ \| '__/ __|
"    \ V /| | | | | | | | | (__
"     \_/ |_|_| |_| |_|_|  \___|
"
" Base Settings   " {{{
set nocompatible   " Must be first line
set encoding=utf8
scriptencoding utf8

set background=dark     " Assume a dark background
set noignorecase       " who ever thought that ignoring case is a good idea?
set nospell            " speller off
set nowrap

set timeout         " don't timeout on mappings
set ttimeout        " do timeout on terminal key codes
set timeoutlen=700  " timeout after 100 msec on mappings
set ttimeoutlen=50 " timeout after 100 msec on keycodes

set backupdir=~/.vim/backup
set directory=~/.vim/swap
set undodir=~/.vim/undo
set viewdir=~/.vim/views

filetype plugin indent on   " Automatically detect file types.
syntax on                   " Syntax highlighting
" }}}

" Load Plugs:  " {{{
if empty(glob('~/.vim/autoload/plug.vim'))
augroup replug
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
augroup END
endif
call plug#begin('~/.vim/plugged')
  " Dependenciess:  " {{{
    Plug 'MarcWeber/vim-addon-mw-utils'
    Plug 'tomtom/tlib_vim'
    Plug 'altercation/vim-colors-solarized'
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
  " }}}
" Vimpager: " {{{
    if exists('vimpager')
      call plug#end()
      set nolist
      set nocursorcolumn
      runtime ftplugin/man.vim
      nmap K :Man <cword><CR>
      augroup vimpager
        autocmd! VimEnter, Colorscheme * highlight! CursorLine ctermbg=0
      augroup END
      finish
    endif
" }}}
  " General:  " {{{
  " FIXME: on-demand lazy loading seems broken
    Plug 'bling/vim-bufferline'
    Plug 'easymotion/vim-easymotion'
    Plug 'gcmt/wildfire.vim'
    Plug 'jiangmiao/auto-pairs'
    Plug 'jistr/vim-nerdtree-tabs' ", , { 'on': 'NERDTreeTabsToggle' }
    Plug 'mbbill/undotree' ", , { 'on': 'UndoTreeToggle' }
    Plug 'mhinz/vim-signify'
    Plug 'nathanaelkane/vim-indent-guides' ", , { 'on': 'IndentGuidesToggle' }
    Plug 'osyo-manga/vim-over'
    Plug 'rhysd/conflict-marker.vim'
    Plug 'scrooloose/nerdtree' ",  { 'on': ['NERDTreeTabsToggle', 'NERDTreeToggle'] }
    Plug 'tpope/vim-abolish'
    Plug 'tpope/vim-repeat'
    Plug 'tpope/vim-surround'
    Plug 'vim-scripts/matchit.zip'
    Plug 'vim-scripts/restore_view.vim'
    Plug 'vim-scripts/sessionman.vim' ", , { 'on': ['SessionList', 'SessionSave', 'SessionClose'] }
  " }}}
  " Writing:" {{{
    Plug 'kana/vim-textobj-indent'
    Plug 'kana/vim-textobj-user'
    Plug 'reedes/vim-litecorrect'
    Plug 'reedes/vim-textobj-quote'
    Plug 'reedes/vim-textobj-sentence'
    Plug 'reedes/vim-wordy'
  " }}}
  " Programming: " {{{
    Plug 'godlygeek/tabular'
    Plug 'luochen1990/rainbow'
    Plug 'majutsushi/tagbar', {'on': 'TagbarToggle'}
    Plug 'mattn/gist-vim'
    Plug 'mattn/webapi-vim'
    Plug 'scrooloose/syntastic'
    Plug 'tpope/vim-commentary'
    Plug 'tpope/vim-fugitive'
  " }}}
  " Snippets:& AutoComplete " {{{
    Plug 'SirVer/ultisnips'
    Plug 'ycm-core/YouCompleteMe', { 'do': './install.py' }
    " Plug 'dense-analysis/ale'
    Plug 'honza/vim-snippets'
  " }}}
  " Python: " {{{
    Plug 'klen/python-mode', { 'for': 'python' }
    Plug 'psf/black' , { 'branch': 'stable'  }
    Plug 'vim-scripts/python_match.vim', { 'for': 'python' }
    Plug 'vim-scripts/pythoncomplete', { 'for': 'python' }
    Plug 'yssource/python.vim', { 'for': 'python' }
  " }}}
  " Javascript: " {{{
    Plug 'briancollins/vim-jst', { 'for': ['json', 'javascript'] }
    Plug 'elzr/vim-json', { 'for': 'json' }
    Plug 'groenewege/vim-less', { 'for': 'javascript' }
    Plug 'kchmck/vim-coffee-script', { 'for': 'javascript' }
    Plug 'pangloss/vim-javascript', { 'for': 'javascript' }
  " }}}
  " HTML:      " {{{
    Plug 'gorodinskiy/vim-coloresque', { 'for': ['html', 'css'] }
    Plug 'hail2u/vim-css3-syntax', { 'for': ['html', 'css'] }
    Plug 'mattn/emmet-vim', { 'for': 'html' }
    Plug 'tpope/vim-haml', { 'for': 'html' }
  " }}}
  " Misc:      " {{{
    Plug 'cespare/vim-toml', { 'for': 'toml' }
    Plug 'tpope/vim-markdown', { 'for': 'markdown' }
  " }}}
" Standard: "  {{{
    Plug 'PotatoesMaster/i3-vim-syntax', { 'for': 'i3' }
    Plug 'vim-scripts/bufexplorer.zip' ", , { 'on': ['BufExplorerVerticalSplit', 'BufExplorerHorizontalSplit', 'BufExplorer'] }
    Plug 'vim-scripts/Threesome' ", , { 'on': 'ThreesomeInit'}
    Plug 'zaiste/tmux.vim', { 'for': 'tmux' }
  " }}}
call plug#end()
" Solarized:  {{{
  let g:solarized_termcolors=256
  let g:solarized_termtrans=1
  let g:solarized_contrast='normal'
  let g:solarized_visibility='normal'
  let g:airline_theme = 'solarized'
  colorscheme solarized
"  }}}
" }}}

" Settings: " {{{
set autoindent                 " Indent at the same level of the prev. line
set autowrite                  " edit using buffers
set backspace=indent,eol,start " Backspace for dummies
set backup                     " Backups are nice ...
set clipboard=autoselect       " experimenting, try to see if it fixes yank
set colorcolumn=-1,-2,-3,-4,-5 " highlight 5 columns before col8O
set completeopt=menu,preview,longest
set conceallevel=2
set concealcursor=i
set confirm                    " Non UTF-8 Fallback
set cursorcolumn               " cursor crosshairs: nice.
set cursorline                 " Highlight current line
set diffopt=filler,vertical,foldcolumn:4,context:3
set expandtab                  " Tabs are spaces, not tabs
set fillchars=fold:·
set foldcolumn=2               " fold indicator
set foldenable
set foldminlines=1
set foldopen=hor,insert,jump,mark,percent,quickfix,search,tag,undo
set hidden                     " Allow buffer switching without saving
set history=1000               " Store a ton of history (default is 20)
set hlsearch                   " Highlight search terms
set ignorecase                 " Case insensitive search
set incsearch                  " Find as you type search
set iskeyword-=#               " '#' is an end of word designator
set iskeyword-=-               " '-' is an end of word designator
set iskeyword-=.               " '.' is an end of word designator
set laststatus=2               " always have a statys line
set linespace=0                " No extra spaces between rows
set list
set listchars=tab:➔\ ,trail:·,extends:❮,precedes:❯,nbsp:¯
set matchpairs+=<:>            " Match, to be used with %
set mouse=a                    " Automatically enable mouse usage
set mousehide                  " Hide the mouse cursor while typing
set nojoinspaces               " Prevents inserting two spaces on a join (J)
set number                     " Line numbers on
set pastetoggle=<F12>          " pastetoggle (sane indentation on pastes)
set ruler                      " Show the ruler
set rulerformat=%30(%=\:b%n%y%m%r%w\ %l,%c%V\ %P%) " A ruler on steroids
set scrolljump=1               " Smooth scrolling
set scrolloff=1
set sessionoptions=blank,buffers,curdir,folds,tabpages,winsize
set shiftwidth=2               " Use indents of 4 spaces
set shortmess+=filmnrxoOtT     " Abbrev. of messages (avoids 'hit enter')
set showbreak=➘                " Character indicating a wrapped line
set showcmd                    " Show partial commands in status line and
set showmatch                  " Show matching brackets/parenthesis
set showmode                   " Display the current mode
set smartcase                  " Case sensitive when uc present
set softtabstop=2              " Let backspace delete indent
set splitbelow                 " Puts new split windows to the bottom
set splitright                 " Puts new vsplit windows to the right
set statusline+=%=%-14.(%l,%c%V%)\ %p%%    " Right aligned file nav info
set statusline+=%w%h%m%r       " Options
set statusline+=%{fugitive#statusline()}   " Git Hotness
set statusline+=\ [%{&ff}/%Y]  " Filetype
set statusline+=\ [%{getcwd()}] " Current dir
set statusline=%<%f\            " Filename
set tabpagemax=15               " Only show 15 tabs
set tabstop=2                   " An indentation every four columns
set tags=./tags;/,~/.vimtags
set textwidth=80                " 80 col width enforcemen                                                      " CCC
set undofile                    " So is persistent undo ...
set undolevels=1000             " Maximum number of changes that can be undone
set undoreload=10000            " Maximum number lines to save for undo on a buffer reload
set viewoptions=folds,options,cursor,unix,slash
set virtualedit=onemore         " Allow for cursor beyond last character
set whichwrap=b,s,h,l,<,>,[,]   " Backspace and cursor keys wrap too
set wildmenu                    " Show list instead of just completing
set wildmode=list:longest,full  " Command <Tab> completion, list matches, then longest common part, then all.
set winminheight=0              " Windows can be 0 line high
if has('gui_running')
  set columns=85
  set guifont=Monoid\ 9
  set guioptions=Paegm
  set lines=40                " 40 lines of text instead of 24
endif
" }}}

" Key Mappings {{{
" Fix xterm & alacritty  " {{{
if  &term ==# 'xterm'  ||
  \ &term ==# 'screen' ||
  \ &term ==# 'alacritty'
  inoremap <silent> <C-[>OA <Up>
  inoremap <silent> <C-[>OB <Down>
  inoremap <silent> <C-[>OC <Right>
  inoremap <silent> <C-[>OD <Left>
  inoremap <silent> <C-[>OF <End>
  inoremap <silent> <C-[>OH <Home>
endif   " }}}
" Mapleader{{{
    let g:mapleader = ','
    let g:maplocalleader = '_'
"}}}
    " Easier moving in tabs and windows{{{
    map <C-J> <C-W>j<C-W>_
    map <C-K> <C-W>k<C-W>_
    map <C-L> <C-W>l<C-W>_
    map <C-H> <C-W>h<C-W>_
    "}}}
    " Wrapped lines goes down/up to next row, {{{
    noremap j gj
    noremap k gk
    "}}}
    " Stupid shift key fixes{{{
    command! -bang -nargs=* -complete=file E e<bang> <args>
    command! -bang -nargs=* -complete=file W w<bang> <args>
    command! -bang -nargs=* -complete=file Wq wq<bang> <args>
    command! -bang -nargs=* -complete=file WQ wq<bang> <args>
    command! -bang Wa wa<bang>
    command! -bang WA wa<bang>
    command! -bang Q q<bang>
    command! -bang QA qa<bang>
    command! -bang Qa qa<bang>
    cmap Tabe tabe
    " Yank from the cursor to the end of the line, like C, D
    nnoremap Y y$
    "}}}
    " Code folding And Tabs:{{{
    map <S-H> gT
    map <S-L> gt
    " Folds {{{
    nmap <leader>f0 :set foldlevel=0<CR>
    nmap <leader>f1 :set foldlevel=1<CR>
    nmap <leader>f2 :set foldlevel=2<CR>
    nmap <leader>f3 :set foldlevel=3<CR>
    nmap <leader>f4 :set foldlevel=4<CR>
    nmap <leader>f5 :set foldlevel=5<CR>
    nmap <leader>f6 :set foldlevel=6<CR>
    nmap <leader>f7 :set foldlevel=7<CR>
    nmap <leader>f8 :set foldlevel=8<CR>
    nmap <leader>f9 :set foldlevel=9<CR>
    "}}}
    " Tabs  {{{
    nmap <leader>1 <Plug>AirlineSelectTab1
    nmap <leader>2 <Plug>AirlineSelectTab2
    nmap <leader>3 <Plug>AirlineSelectTab3
    nmap <leader>4 <Plug>AirlineSelectTab4
    nmap <leader>5 <Plug>AirlineSelectTab5
    nmap <leader>6 <Plug>AirlineSelectTab6
    nmap <leader>7 <Plug>AirlineSelectTab7
    nmap <leader>8 <Plug>AirlineSelectTab8
    nmap <leader>9 <Plug>AirlineSelectTab9
    "}}}}}}
    " Shortcuts{{{
    " Find merge conflict markers
    map <leader>fc /\v^[<\|=>]{7}( .*\|$)<CR>
    " Change Working Directory to that of the current file
    cmap cwd lcd %:p:h
    cmap cd. lcd %:p:h
    " Visual shifting (does not exit Visual mode)
    vnoremap < <gv
    vnoremap > >gv
    " Allow using the repeat operator with a visual selection (!)
    " http://stackoverflow.com/a/8064607/127816
    vnoremap . :normal .<CR>
    " For when you forget to sudo.. Really Write the file.
    cmap w!! w !sudo tee % >/dev/null
    " spanish keyboard not frieldy for help tag navigation
    nmap <silent> <leader>+ <C-]>
    nmap <silent> <leader>¡ <C-T>
    " }}}
    " Some helpers to edit mode{{{
    " http://vimcasts.org/e/14
    cnoremap %% <C-R>=fnameescape(expand('%:h')).'/'<cr>
    map <leader>ew :e %%
    map <leader>es :sp %%
    map <leader>ev :vsp %%
    map <leader>et :tabe %%
    " Adjust viewports to the same size
    map <Leader>= <C-w>=
    " Map <Leader>ff to display all lines with keyword under cursor
    " and ask which one to jump to
    nmap <Leader>ff [I:let nr = input('Which one: ')<Bar>exe 'normal ' . nr .'[\t'<CR>
    " Easier horizontal scrolling
    map zl zL
    map zh zH
    " Easier formatting
    nnoremap <silent> <leader>q gwip
    "}}}
"  Original    {{{
    " Repeat last ex command
    nnoremap ! @:
    "column separatopr and date separator insert for a whole column
    nnoremap <silent> <leader>!  :ColSep<CR>
    nnoremap <silent> <leader>@  :DateSep<CR>
    nnoremap <silent> <leader>cr :ReloadHloConfig<CR>
    nnoremap <silent> <leader>do :DiffOrig<CR>
    " Capitalize every word.
    nnoremap <silent> <leader>C  :s/\<\a/\u&/g<CR>:nohlsearch<CR>
    "turn off pesky search highligmht
    nnoremap <silent> <leader>l  :nohlsearch<CR>
    nnoremap <silent> <leader>ll :AppendEditLogLine<CR>
    "which key does what? awesome
    nnoremap <silent> <leader>mm :TBrowseOutput map<CR>
    nnoremap <silent> <leader>mn :TBrowseOutput nmap<CR>
    nnoremap <silent> <leader>mi :TBrowseOutput imap<CR>
    nnoremap <silent> <leader>mv :TBrowseOutput vmap<CR>
    " head off to column 80? why? 'cause its fun, yo
    nnoremap <silent> <leader>mc :normal 80\|<CR>
    " get rid of trailing spaces
    nnoremap <silent> <leader>s  :%s/\s\+$//g<CR>
    nnoremap <silent> <leader>ts :call setline(line('.'), strftime('%Y%m%d %H%M%S %z %Z %s'))<CR>
    nnoremap <silent> <leader>tS :call setline(line('.'),printf(' h@h-lo.me %s %s ', strftime('%Y%m%d %H%M%S %z %Z %s'), 'd(-_- )b...' ))<CR>
    "clipboard cut and paste, cuz im lazy.
    nnoremap <silent> <leader>c  :normal "+y
    nnoremap <silent> <leader>p  :normal "+p
    nnoremap <silent> <leader>v  :normal "+p
    nnoremap <silent> <leader>y  :normal "+y
    "insert whatever is on the clipboard forcefully, behind tmux
    nnoremap <silent> <leader><C-V> :normal r! xclip -o<CR>
"}}}
  " FKey Toggles: " {{{
        noremap  <F2>   :DelimitMateSwitch<CR>
        noremap  <F3>   :UndoTreeToggle<CR>
        nnoremap <Leader>u :UndotreeToggle<CR>
        if &diff
          nnoremap <F3> :diffupdate<CR>
        endif
        noremap  <F4>   :set wrap!
        noremap  <S-F4> :set list!
        noremap  <F6>   :IndentGuidesToggle<CR>
        noremap  <F7>   :GitGutterToggle<CR>
        noremap  <F8>   :set number!<CR>
        noremap  <F9>   :set relativenumber!<CR>
        noremap  <F10>  :Black<CR>
  " }}} end toggles
    " JSON  " {{{
        nmap <leader>jt <Esc>:%!python -m json.tool<CR><Esc>:set filetype=json<CR>
        nmap <leader>Jt <Esc>:%!jq . <CR><Esc>:set filetype=json<CR>
        let g:vim_json_syntax_conceal = 0
    " }}}
" }}}

" Plugins    " {{{
    " Airline: {{{
      let g:airline#extensions#branch#displayed_head_limit = 10
      let g:airline#extensions#branch#empty_message = ''
      let g:airline#extensions#branch#enabled = 1
      let g:airline#extensions#branch#use_vcscommand = 0
      let g:airline#extensions#bufferline#enabled = 0
      let g:airline#extensions#bufferline#overwrite_variables = 0
      let g:airline#extensions#csv#column_display = 'Name'
      let g:airline#extensions#csv#enabled = 1
      let g:airline#extensions#quickfix#location_text = 'Location'
      let g:airline#extensions#quickfix#quickfix_text = 'Quickfix'
      let g:airline#extensions#tabline#buffer_idx_mode = 1
      let g:airline#extensions#tabline#enabled = 1
      let g:airline#extensions#tabline#show_buffers = 1
      let g:airline#extensions#tabline#show_tab_nr = 1
      let g:airline#extensions#tabline#show_tabs = 1
      let g:airline#extensions#tabline#tab_nr_type = 1 " tab number
      let g:airline#extensions#tagbar#enabled = 1
      let g:airline#extensions#tagbar#flags = ''
      let g:airline#extensions#virtualenv#enabled = 1
      let g:airline#extensions#whitespace#enabled = 0
      let g:bufferline_echo = 0
    " }}}
    " AutoCloseTag  " {{{
        " Make it so AutoCloseTag works for xml and xhtml files as well
        augroup xhtmlxml
          autocmd!
          autocmd FileType xhtml,xml ru ftplugin/html/autoclosetag.vim
        augroup END
        nmap <Leader>ac <Plug>ToggleAutoCloseMappings
    " }}}
    " Ctags  " {{{
        " Make tags placed in .git/tags file available in all levels of a repository
        let g:gitroot = substitute(system('git rev-parse --show-toplevel'), '[\n\r]', '', 'g')
        if g:gitroot !=# ''
            let &tags = &tags . ',' . g:gitroot . '/.git/tags'
        endif
    " }}}
    " DelimitMate: " {{{
         " loaded_delimitMate=            ... " Turns off the script.
         " delimitMate_autoclose=             " Tells delimitMate whether to automagically insert the closing delimiter.
         " delimitMate_matchpairs=            " Tells delimitMate which characters are matching  pairs.
         " delimitMate_quotes=                " Tells delimitMate which quotes should be used.
         " delimitMate_nesting_quotes=        " Tells delimitMate which quotes should be allowed  to be nested.
         " delimitMate_expand_cr=             " Turns on/off the expansion of <CR>.
         " delimitMate_expand_space=          " Turns on/off the expansion of <Space>.
         " delimitMate_jump_expansion=        " Turns on/off jumping over expansions.
         " delimitMate_smart_quotes=          " Turns on/off the ''smart quotes feature.
         " delimitMate_smart_matchpairs=      " Turns on/off the smart matchpairs feature.
         " delimitMate_balance_matchpairs=    " Turns on/off the balance matching pairs feature.
         " delimitMate_excluded_regions=      " Turns off the script for the given regions or syntax group names.
         " delimitMate_excluded_ft=           " Turns off the script for the given file types.
         " delimitMate_eol_marker=            " Determines what to insert after the closing matchpair when typing an opening matchpair on the end of the line.
    " }}}
    " Fugitive: " {{{
        nnoremap <silent> <leader>gs :Gstatus<CR>
        nnoremap <silent> <leader>gd :Gdiff<CR>
        nnoremap <silent> <leader>gc :Gcommit<CR>
        nnoremap <silent> <leader>gb :Gblame<CR>
        nnoremap <silent> <leader>gl :Glog<CR>
        nnoremap <silent> <leader>gp :Git push<CR>
        nnoremap <silent> <leader>gr :Gread<CR>
        nnoremap <silent> <leader>gw :Gwrite<CR>
        nnoremap <silent> <leader>ge :Gedit<CR>
        nnoremap <silent> <leader>gi :Git add -p %<CR>
        nnoremap <silent> <leader>gg :SignifyToggle<CR>
    " }}}
    " indent_guides  " {{{
        let g:indent_guides_auto_colors = 1
        let g:indent_guides_enable_on_vim_startup = 1
        let g:indent_guides_exclude_filetypes = ['help', 'nerdtree' , 'man']
        let g:indent_guides_guide_size = 1
        let g:indent_guides_start_level = 6
    " }}}
    " Misc  " {{{
        runtime ftplugin/man.vim
        nmap K :Man <cword><CR>
        let g:snips_author = 'Héctor Lecuanda <hector@lecuanda.com>'
        let g:rainbow_active = 1 "0 if you want to enable it later via :RainbowToggle
        let b:match_ignorecase = 1
        nnoremap <silent> <leader>tt :TagbarToggle<CR>
    " }}}
    " NerdTree  " {{{
        map <C-e> <plug>NERDTreeTabsToggle<CR>
        map <leader>e :NERDTreeFind<CR>
        nmap <leader>nt :NERDTreeFind<CR>
        let g:NERDRemoveExtraSpaces=1
        let g:NERDShutUp=1
        let g:NERDSpaceDelims=1
        let g:NERDTreeChDirMode=0
        let g:NERDTreeIgnore=['\.py[cd]$', '\~$', '\.swo$', '\.swp$', '^\.git$', '^\.hg$', '^\.svn$', '\.bzr$']
        let g:NERDTreeKeepTreeInNewTab=1
        let g:NERDTreeMouseMode=2
        let g:NERDTreeQuitOnOpen=1
        let g:NERDTreeShowBookmarks=1
        let g:NERDTreeShowHidden=1
        let g:nerdtree_tabs_autoclose = 1               " Close current tab if there is only one window in it and it's NERDTree
        let g:nerdtree_tabs_focus_on_files = 0          " When switching into a tab, make sure that focus is on the file window,
        let g:nerdtree_tabs_meaningful_tab_names = 1    " Unfocus NERDTree when leaving a tab for descriptive tab names
        let g:nerdtree_tabs_open_on_console_startup = 0 " Open NERDTree on console vim startup
        let g:nerdtree_tabs_open_on_gui_startup = 0     " Open NERDTree on gvim/macvim startup
        let g:nerdtree_tabs_open_on_new_tab = 1         " Open NERDTree on new tab creation (if NERDTree was globally opened by :NERDTreeTabsToggle)
        let g:nerdtree_tabs_synchronize_view = 1        " Synchronize view of all NERDTree windows (scroll and cursor position)
    " }}}
    " OmniComplete  " {{{
            if has("autocmd") && exists("+omnifunc")
              autocmd Filetype *
                \if &omnifunc == "" |
                \setlocal omnifunc=syntaxcomplete#Complete |
                \endif
            endif
          " Hiding the popup menu {{{
            inoremap <expr> <Esc>      pumvisible() ? "\<C-e>" : "\<Esc>"
            inoremap <expr> <CR>       pumvisible() ? "\<C-y>" : "\<CR>"
            inoremap <expr> <Down>     pumvisible() ? "\<C-n>" : "\<Down>"
            inoremap <expr> <Up>       pumvisible() ? "\<C-p>" : "\<Up>"
            inoremap <expr> <C-d>      pumvisible() ? "\<PageDown>\<C-p>\<C-n>" : "\<C-d>"
            inoremap <expr> <C-u>      pumvisible() ? "\<PageUp>\<C-p>\<C-n>" : "\<C-u>"
          " }}}
          augroup oncursormove
            autocmd!
            autocmd CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
            autocmd CursorMoved,InsertEnter  * silent execute 'nohlsearch'
          augroup END
    " }}}
    " PyMode  " {{{
        let g:pymode_lint_checkers = ['pyflakes']
        let g:pymode_trim_whitespaces = 0
        let g:pymode_options = 0
        let g:pymode_rope = 0
    " }}}
    " SessionManager:  " {{{
        nmap <leader>sl :SessionList<CR>
        nmap <leader>ss :SessionSave<CR>
        nmap <leader>sc :SessionClose<CR>
    " }}}
    " UndoTree:  " {{{
        let g:undotree_SetFocusWhenToggle=1
        let g:undotree_DiffAutoOpen =1
        let g:undotree_DiffPanelHeight=0
        let g:undotree_SetFocusWhenToggle = 1
        let g:undotree_HighlightChangedText = 1
    " }}}
    " Wildfire  " {{{
    let g:wildfire_objects = {
    \ '*' : ["i'", 'i"', 'i)', 'i]', 'i}', 'ip'],
    \ 'html,xml' : ['at'],
    \ }
    map <SPACE> <Plug>(wildfire-fuel)
    vmap <C-SPACE> <Plug>(wildfire-water)
    nmap <leader>s <Plug>(wildfire-quick-select)
    " }}}
    " YouCompleteMe: " {{{
            let g:acp_enableAtStartup = 0
            let g:ycm_collect_identifiers_from_tags_files = 1
          " let g:ycm_seed_identifiers_with_syntax = 1
          " let g:ycm_collect_identifiers_from_tags_files = 1
          " let g:ycm_always_populate_location_list = 1
          " let g:ycm_collect_identifiers_from_comments_and_strings = 1
          " let g:ycm_add_preview_to_completeopt = 1
          " let g:ycm_autoclose_preview_window_after_completion = 1
          " let g:ycm_autoclose_preview_window_after_insertion = 1
          " let g:ycm_key_list_select_completion = ['<TAB>', '<Down>']
          " let g:ycm_key_list_previous_completion = ['<S-TAB>', '<Up>']
          " let g:ycm_key_invoke_completion = '<C-Space>'
          " let g:ycm_key_detailed_diagnostics = '<leader>d'
          " remap Ultisnips for compatibility for YCM{{{
            let g:UltiSnipsExpandTrigger = '<C-j>'
            let g:UltiSnipsJumpForwardTrigger = '<C-j>'
            let g:UltiSnipsJumpBackwardTrigger = '<C-k>'
          "}}}
    " }}}
" }}}

" Functions  " {{{
    function! NERDTreeInitAsNeeded() " {{{
        redir => l:bufoutput
        buffers!
        redir END
        let l:idx = stridx(l:bufoutput, 'NERD_tree')
        if l:idx > -1
            NERDTreeMirror
            NERDTreeFind
            wincmd l
        endif
    endfunction
    " }}}
    function! StripTrailingWhitespace()   " {{{
        " Preparation: save last search, and cursor position.
        let l:_s=@/
        let l:l = line('.')
        let l:c = col('.')
        " do the business:
        execute '%s/\s\+$//e'
        " clean up: restore previous search history, and cursor position
        let @/=l:_s
        call cursor(l:l, l:c)
    endfunction
    " }}}
    function! RunShellCommand(cmdline) " {{{
        botright new

        setlocal buftype=nofile
        setlocal bufhidden=delete
        setlocal nobuflisted
        setlocal noswapfile
        setlocal nowrap
        setlocal filetype=shell
        setlocal syntax=shell

        call setline(1, a:cmdline)
        call setline(2, substitute(a:cmdline, '.', '=', 'g'))
        execute 'silent $read !' . escape(a:cmdline, '%#')
        setlocal nomodifiable
        1
    endfunction

    command! -complete=file -nargs=+ Shell call s:RunShellCommand(<q-args>)
    " e.g. Grep current file for <search_term>: Shell grep -Hn <search_term> %
    " }}}
    function! ExpandFilenameAndExecute(command, file) " {{{
        execute a:command . ' ' . expand(a:file, ':p')
    endfunction
" }}}
    function! ResCur()   " {{{
      if line("'\"") <= line('$')
        silent! normal! g`"
        return 1
      endif
    endfunction  " }}}
        function! FixSolarized()  " {{{
          " {{{ Documentation
          " 'base03': 234, 'base0': 244, 'yellow':  136,  'violet': 61, 'black': 16,
          " 'base02': 235, 'base1': 245, 'orange':  166,  'blue':   33, 'dark':  8,
          " 'base01': 239, 'base2': 187, 'red':     124,  'cyan':   37, 'bright': 0,
          " 'base00': 240, 'base3': 230, 'magenta': 125,  'green':  64  }}}
          highlight! ColorColumn   ctermbg=0
          highlight! Comment                               cterm=italic
          highlight! CursorColumn  ctermbg=0
          highlight! CursorLine    ctermbg=0
          highlight! CursorLineNr              ctermfg=255
          highlight! FoldColumn    ctermbg=16
          highlight! Folded        ctermbg=16
          highlight! IncSearch     ctermbg=0   ctermfg=15  cterm=italic
          highlight! LineNr        ctermbg=0
          highlight! PmenuSel      ctermbg=8   ctermfg=255
          highlight! Search        ctermbg=4   ctermfg=15  cterm=italic
          highlight! SignColumn    ctermbg=0
          highlight! VertSplit     ctermbg=16
        endfunction FixSolarized " }}}
" }}}

" Custom Commands: " {{{
    " source ~/Repos/dotfiles/bin/vim-scripts/passport.vim
    " InsExCmd: " {{{   Capture ex command output
    " Arguments:
        function! InsExCmd(cmd)
          redir => l:message
          silent execute a:cmd
          redir END
          silent put l:message
          set nomodified
        endfunction
        command! -nargs=+ -complete=command InsExCmd call InsExCmd(<q-args>)
    " }}}
    " ColSep: " {{{ Insert a column separator at the current column.
    " Arguments:
        function! ColSep()
            let l:splitcol = col('.')
            let l:subspattern = '\%' . l:splitcol . 'c'
            execute '%s/' . l:subspattern . '/|/'
            :1
            execute 'normal '. l:splitcol . '|'
        endfunction
        command! -nargs=0 -complete=command ColSep call ColSep()
    " }}}
    " DateSep: " {{{  Insert a column separator at the current column.
    " Arguments:
        function! DateSep()
            let l:splitcol = col('.')
            let l:subspattern = '\%' . l:splitcol . 'c'
            execute '%s/' . l:subspattern . '/-/'
            :1
            execute 'normal '. l:splitcol . '-'
        endfunction
        command! -nargs=0 -complete=command DateSep call DateSep()
    " }}}
    " CobolSignFlip: " {{{ Flip the trailing sign to a leading sign and split columns with pipe
    " Arguments:
        function! CobolSignFlip()
            let l:splitcol = col('.')
            let l:subspattern = '\%>' . l:splitcol . 'c\(\d\{4,\}\)\([+-]\)'
            execute '%s/' . l:subspattern . '/\2\1|/'
            :1
            execute 'normal '. l:splitcol . '|'
        endfunction
        command! -nargs=0 -complete=command CobolSignFlip call CobolSignFlip()
    " }}}
    " HloCleanTrace: " {{{  make a verbose file more legible
    " Arguments:
        function! HloCleanTrace()
            g/^line/d
            g/previous dir/d
            g/finished/d
            g/continuing/d
            g/^Searching/d
        endfunction
        command! -nargs=0 -complete=command HloCleanTrace call HloCleanTrace()
    " }}}
    " HloTitle: " {{{ Desc: Creates a nice ascii-art title for scripts
    "Arguments: N/A
        function! HloTitle(title)
            let l:fname=expand('%:t')
            let l:totallines=line('$')
            call append(1,'#')
            call cursor(3,1)
            execute ':r! figlet -cw 80 ' . a:title
            let l:newTotalLines=line('$')
            let l:increase=l:totallines - l:newTotalLines
            call setline(l:increase+1,strftime(localtime()))
            call setline(l:increase+2,l:fname)
            let l:titlerange=l:increase+3
            call cursor(2,1)
            execute '2,' . l:titlerange . 'gcc'
            execute '%s/\s\+$//g'
        endfunction
        command! -nargs=1 -complete=command HloTitle silent call HloTitle(<q-args>)
    " }}}
    " ReloadHloConfig: " {{{ Reload local configs to vim
        function! ReloadHloConfig()
            source ~/src/Dotfiles/dotfiles/vim.d/vimrc.before.local
            source ~/src/Dotfiles/dotfiles/vim.d/vimrc.local
        endfunction
        command! -nargs=0 -complete=command ReloadHloConfig silent call ReloadHloConfig()
    " }}}
    " AppendEditLogLine: " {{{ append a timestamp comment
        function! AppendEditLogLine() abort
            let l:mlcoment = printf(' h@h-lo.me %s %s ', strftime('%Y%m%d %H%M%S %z %Z %s'), 'd(-_- )b...' )
            let l:blnkline = substitute(&commentstring, '%s', l:mlcoment, '')
            call append(line('$')-1, l:blnkline)
            call cursor(line('$')-1)
        endfunction
        command! -nargs=0 -complete=command AppendEditLogLine silent call AppendEditLogLine()
    " }}}
    " AppendModeline: " {{{ Append modeline after last line in buffer.
        function! AppendModeline()
            let l:modeline = printf(' vim: set ft=%s sw=%d tw=%d fdm=%s %set :', &filetype, &shiftwidth, &textwidth, &foldmethod, &expandtab ? '' : 'no')
            let l:modeline = substitute(&commentstring, '%s', l:modeline, '')
            call append(line('$'), l:modeline)
            call cursor(line('$'))
        endfunction
        nnoremap <silent> <Leader>ml :call AppendModeline()<CR>
    " }}}
    " DiffOrig: " {{{ Highlight changes vs original file
        function! DiffOrig()
            vert new
            set buftype=nofile
            r ++edit #
            0d_
            diffthis
            wincmd p
            diffthis
        endfunction
        command! -nargs=0 -complete=command DiffOrig silent call DiffOrig()
    " command! DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis | wincmd p | diffthis
    " }}}
 " }}}

 " Autocommands: " {{{
            augroup omnicompletion " {{{
              autocmd!
              autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
              autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
              autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
              autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
              autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
              autocmd FileType ruby setlocal omnifunc=rubycomplete#Complete
            augroup END " }}}
            augroup linters  "{{{
              autocmd!
              autocmd BufWritePre *.py execute ':Black'
              autocmd FileType javascript set formatprg=prettier-eslint\ --stdin
            augroup END      " }}}
        augroup fixsolarized " {{{
          autocmd!
          autocmd VimEnter    * :call FixSolarized()
          autocmd Colorscheme * :call FixSolarized()
        augroup END " }}}
        augroup cursorcrossHairsa " {{{
          autocmd!
          autocmd WinEnter * setlocal cursorcolumn
          autocmd WinEnter * setlocal cursorline
          autocmd WinLeave * setlocal nocursorcolumn
          autocmd WinLeave * setlocal nocursorline
        augroup END " }}}
      augroup droptrailingspaces "  {{{
        autocmd!
        autocmd BufWritePre * %s/\s\+$//e
      augroup END " }}}
    augroup gitcommit   "{{{
      autocmd!
      autocmd FileType gitcommit autocmd! BufEnter COMMIT_EDITMSG call setpos('.', [0, 1, 1, 0])
    augroup ENDi        "}}}
    augroup resCur      "{{{
      autocmd!
      autocmd BufWinEnter * call ResCur()
    augroup END         "}}}
    augroup AutoChDira   "{{{  Always switch to the current file directory
      autocmd!
      autocmd BufEnter * if bufname('') !~ '^\[A-Za-z0-9\]*://' | lcd %:p:h | endif
    augroup END "}}}
    augroup textobj_sentence  "{{{
      autocmd!
      autocmd FileType markdown call textobj#sentence#init()
      autocmd FileType textile call textobj#sentence#init()
      autocmd FileType text call textobj#sentence#init()
    augroup END  "}}}
    augroup textobj_quote  "{{{
        autocmd!
        autocmd FileType markdown call textobj#quote#init()
        autocmd FileType textile call textobj#quote#init()
        autocmd FileType text call textobj#quote#init({'educate': 0})
    augroup END  "}}}
" }}} end autocommands

" Terminal And GUI: " {{{
        if (&term ==# 'ansi')  "  {{{
            let g:ycm_warning_symbol = '!!'
            let g:ycm_error_symbol = '>>' " }}}
        else " {{{ Symbols:
          if !exists('g:airline_symbols')
                let g:airline_symbols =  { }
          endif
          let g:airline_left_alt_sep = '' " powerline
          let g:airline_left_sep = '' " powerline
          let g:airline_right_alt_sep = '' " powerline
          let g:airline_right_sep = '' " powerline
          let g:airline_symbols.branch = '' " powerline
          let g:airline_symbols.linenr = '' " powerline
          let g:airline_symbols.maxlinenr = '☰' " unicode
          let g:airline_symbols.notexists = '∄' " unicode
          let g:airline_symbols.spell = 'Ꞩ' " unicode
          let g:airline_symbols.whitespace = 'Ξ' " unicode
        endif " }}}
" }}} end Terminal and GUI

" vim: set ft=vim sw=2 tw=78 fdm=marker fml=1 et :
