#!/usr/bin/env make

SHELL     = zsh

ALL      != fd --type f vimrc .
CTAGS     = ctags -R --languages=Vim --languages=Python -o $@
LIBS      = ~/.vim/bundle
LINTER    = vint
VIMBASE   = $(HOME)/.vim
VIMDIRS   = backup swap undo views bundle
ALLD     := $(addprefix $(VIMBASE)/, $(VIMDIRS))
ALLDIRS  := $(addsuffix /.notempty,  $(ALLD))
PLUGREPO  = https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

.SUFFIXES:

.SUFFIXES: vim

.PHONY: lint outputs install

lint:
	@$(LINTER) vimrc

clean:
	$(RM) old new .install
	$(RM) $(ALLDIRS)

tags: vimrc
	$(CTAGS) $<  $(LIBS)

old new: vimrc Makefile
	mv ~/.vimrc ~/.vimrc.bak
	cp vimrc ~/.vimrc
	vim -V15new +q
	mv ~/.vimrc.bak ~/.vimrc
	vim -V15old +q

outputs: old new
	vimdiff old new

install: $(HOME)/.vimrc | plug

plug: $(VIMBASE)/autoload/plug.vim

$(HOME)/.vimrc: vimrc | $(ALLDIRS)
	install --backup=numbered -vT $< $@

$(VIMBASE)/autoload/plug.vim:
	curl -fLo $@ --create-dirs $(PLUGREPO)

$(ALLDIRS): $(VIMBASE)/%/.notempty: .notempty
	install -D $< $@

.notempty:
	touch $@

.safe: | lint
	cp -Rav vimrc .safe

%:

#  d(-_-;)bm  hlo.mx 1620539959
#  vim:  tw=80 noet :
